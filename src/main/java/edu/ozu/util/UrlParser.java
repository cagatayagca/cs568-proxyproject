package edu.ozu.util;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class UrlParser {

	private UrlParser() {
	}

	public static String parseUrl(String url, byte[] responseByteArray, String baseHrefUrl, List<String> logsToClient)
			throws MalformedURLException, UnsupportedEncodingException {
		String baseUrl = getRootPath(url);
		Document doc = Jsoup.parse(new String(responseByteArray, StandardCharsets.UTF_8.name()), baseUrl);

		for (Element element : doc.getAllElements()) {
			if (element.hasAttr("href")) {
				element.attr("href", replaceWithUrl(element.attr("href"), baseUrl, baseHrefUrl, logsToClient));
			}
			if (element.hasAttr("src")) {
				element.attr("src", replaceWithUrl(element.attr("src"), baseUrl, baseHrefUrl, logsToClient));
			}
			if (element.hasAttr("content")) {
				if (element.attr("content").contains("."))
					element.attr("content",
							replaceWithUrl(element.attr("content"), baseUrl, baseHrefUrl, logsToClient));
			}
		}

		return doc.html();
	}

	private static String getRootPath(String url) throws MalformedURLException {
		URL urlInstance = new URL(url);
		return urlInstance.getProtocol() + "://" + urlInstance.getHost() + ":"
				+ (urlInstance.getPort() == -1 ? "https".equals(urlInstance.getProtocol()) ? 443 : 80
						: urlInstance.getPort())
				+ "/";
	}

	public static String replaceWithUrl(String formerPath, String url, String baseHrefUrl, List<String> logsToClient)
			throws MalformedURLException {
		String finalUrl = "";
		String logSentence = "";
		if (formerPath.startsWith("http")) {
			finalUrl = baseHrefUrl + formerPath;
			logSentence = formerPath + " IS CONVERTED TO -> " + finalUrl;
			if (!logsToClient.contains(logSentence))
				logsToClient.add(logSentence);
		} else if (formerPath.startsWith("//")) {
			URL urlInstance = new URL(url);
			finalUrl = baseHrefUrl + urlInstance.getProtocol() + ":" + formerPath;
			logSentence = formerPath + " IS CONVERTED TO -> " + finalUrl;
			if (!logsToClient.contains(logSentence))
				logsToClient.add(logSentence);
		} else {
			if (formerPath.startsWith("/")) {
				formerPath = formerPath.substring(1);
			}
			finalUrl = baseHrefUrl + getRootPath(url) + formerPath;
			logSentence = formerPath + " IS CONVERTED TO -> " + finalUrl;
			if (!logsToClient.contains(logSentence))
				logsToClient.add(logSentence);
		}

		return finalUrl;
	}

}
