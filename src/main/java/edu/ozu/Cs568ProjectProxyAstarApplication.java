package edu.ozu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Cs568ProjectProxyAstarApplication {

	public static void main(String[] args) {
		SpringApplication.run(Cs568ProjectProxyAstarApplication.class, args);
	}

}

