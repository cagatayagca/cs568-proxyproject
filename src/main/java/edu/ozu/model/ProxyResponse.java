package edu.ozu.model;

import java.util.List;

public class ProxyResponse {

	private List<String> log;
	private String html;

	public List<String> getLog() {
		return log;
	}

	public void setLog(List<String> log) {
		this.log = log;
	}

	public String getHtml() {
		return html;
	}

	public void setHtml(String html) {
		this.html = html;
	}

	@Override
	public String toString() {
		return String.format("%s - HTML dosyasinin ilk 10 karakteri: %s", log, html.subSequence(0, 10));
	}

}
