package edu.ozu.controller;

import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import edu.ozu.model.ProxyResponse;
import edu.ozu.util.UrlParser;

@RestController
public class MainRESTController {

	@Value("${baseHrefUrl}")
	private String baseHrefUrl;

	private List<String> logsToClient;

	@SuppressWarnings("deprecation")
	@PostMapping(value = "/redirect")
	public ProxyResponse getWebSiteViaProxy(
			@RequestParam(value = "webSite", defaultValue = "https://www.google.com") String webSite) {
		ProxyResponse proxyResponse = new ProxyResponse();
		logsToClient = new ArrayList<>();
		try {

			proxyResponse
					.setHtml(
							UrlParser.parseUrl(webSite,
									IOUtils.toByteArray(new InputStreamReader(new URL(webSite).openStream(),
											Charset.forName(StandardCharsets.UTF_8.name()))),
									baseHrefUrl, logsToClient));
			proxyResponse.setLog(logsToClient);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return proxyResponse;
	}

}
