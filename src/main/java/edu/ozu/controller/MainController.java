package edu.ozu.controller;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import edu.ozu.util.UrlParser;

@Controller
public class MainController {

	@Value("${baseHrefUrl}")
	private String baseHrefUrl;

	@GetMapping(value = "/")
	public String welcome() {
		return "index";
	}

	@GetMapping("/content")
	public ResponseEntity<?> getContent(@RequestParam(name = "url") String url) throws IOException {

		List<String> dummyLogs = new ArrayList<>();
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders httpHeaders = new HttpHeaders();
		ResponseEntity<?> responseEntity = null;
		HttpEntity<?> requestEntity = new HttpEntity<Object>(httpHeaders);
		httpHeaders.add(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, "*");
		httpHeaders.add(HttpHeaders.ACCEPT_CHARSET, StandardCharsets.UTF_8.name());
		MediaType responseMediaType = null;
		ResponseEntity<byte[]> responseByteArray = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
				byte[].class);
		responseMediaType = responseByteArray.getHeaders().getContentType();
		if (MediaType.TEXT_HTML.getType().equals(responseMediaType.getType())) {
			try {
				responseEntity = new ResponseEntity<>(
						UrlParser.parseUrl(url, responseByteArray.getBody(), baseHrefUrl, dummyLogs), httpHeaders,
						responseByteArray.getStatusCode());

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			responseEntity = new ResponseEntity<>(responseByteArray.getBody(), httpHeaders,
					responseByteArray.getStatusCode());
		}

		httpHeaders.putAll(responseByteArray.getHeaders());

		return responseEntity;

	}

}
