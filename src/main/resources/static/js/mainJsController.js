var app = angular.module('app', [ 'ngSanitize' ]);
app.controller('controller', [
		'$scope',
		'$http',
		'$sce',
		function($scope, $http, $sce) {

			$scope.urlToRedirectViaProxy = "";
			$scope.htmlContentFromService = "";
			$scope.htmlLogsFromService = "";
			$scope.hideLoadingSpinner = true;

			$scope.redirectViaProxy = function() {
				var iframe = document.getElementById('iframe1');
				iframe.src = "about:blank";

				if ("" == $scope.urlToRedirectViaProxy) {
					alert("Please enter a valid website URL to access.");
				}
				var method = "POST";
				var url = "/redirect?webSite=" + $scope.urlToRedirectViaProxy;
				var data = "";
				var config = {
					headers : {
						'Accept' : '*/*'
					}
				}
				$scope.hideLoadingSpinner = false;
				$http.post(url, data, config).then(
						function(response) {
							$scope.htmlContentFromService = $sce
									.trustAsHtml(response.data.html);
							$scope.htmlLogsFromService = response.data.log;
							iframe.contentWindow.document
									.write($scope.htmlContentFromService);
							$scope.hideLoadingSpinner = true;
						}, function error(response) {
							alert("error: " + response.statusText);
							$scope.hideLoadingSpinner = true;
						});

			};

		} ]);
